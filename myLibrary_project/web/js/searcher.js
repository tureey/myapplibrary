$(document).ready(function(){
    $('#buscar').click(function(){
        Buscar($('#searcher').val());
    });

});

function Buscar(sCadena){
    var sCadenaSplitted = sCadena.split(" ");
    var sTexto = ""; // Inicializamos el texto
    var APIkey = "&key="+"AIzaSyDypzE7kc_dVV2rnRz425gkGkL05yj9qt0"; // APIkey de Google API
    var urlAPI = "https://www.googleapis.com/books/v1/volumes?q=";
    var opcionBusqueda;

    for(var i=0; i<sCadenaSplitted.length; i++)
    {
        if(i === sCadenaSplitted.length-1)
            sTexto += sCadenaSplitted[i];      // En caso de ser la última no lo añadiremos
        else
            sTexto += sCadenaSplitted[i]+"+";  // Ponemos todas las palabras separadas por un +
    }

    opcionBusqueda = $('input[name=opcionBusqueda]:checked').val(); // Miramos que valor tiene el radiobutton

    // Depende de la opcion de busqueda los parametros para la API Books variarán
    if(opcionBusqueda == 'titulo')
        opcionBusqueda = 'intitle';
    else if(opcionBusqueda == 'autor')
        opcionBusqueda = 'inauthor';
    else
        opcionBusqueda = 'ebook';

    $.ajax({
        url: urlAPI + opcionBusqueda + ":" + sTexto,
        type: "GET",

        success: function(res){
            var book;
            var books = res.items.slice(1,21);

            $('#books-list header').remove();                  // Borramos 'Resultados encontrados:'
            $('#books-list ul.collapsible li.book').remove();  // Borramos todas las búsquedas hasta ahora

            books.forEach(function(book){
                var sCategorias = "";   // String que contiene todas las categorias de dicho libro
                var sAutores = "";      // String que contiene todos los autores de dicho libro
                var publishedYear= "";

                book.volumeInfo.categories.forEach(function(categoria){    sCategorias = categoria + ",";  });
                book.volumeInfo.authors.forEach(function(autor){   sAutores = autor + ","; });
                sCategorias = sCategorias.slice(0, -1); // Cortamoas el último caacter porque sobre una ','
                sAutores    = sAutores.slice(0, -1);    // Cortamoas el último caacter porque sobre una ','


                publishedYear = book.volumeInfo.publishedDate.split("-");

                $('#books-list ul.collapsible').append('<li id="' + book.id + '" class="book">'
                    +'<div class="collapsible-header">'
                        +'<img src='+ book.volumeInfo.imageLinks.smallThumbnail + 'alt="Portada del libro">'
                        +'<h2>' + book.volumeInfo.title + '</h2>'
                        +'<i class="fa fa-plus-circle" aria-hidden="true"></i>'
                    +'</div>'
                    +'<div class="collapsible-body">'
                        +'<p class="genero">Género:<span class="bold">'+ sCategorias +'</span></p>'
                        +'<p class="autor">Autor:<span class="bold">'+ sAutores +'</span></p>'
                        +'<p class="idioma">Idioma:<span class="bold">' + book.volumeInfo.language + '</span></p>'
                        +'<p class="publicacion"><span class="grey">' + book.volumeInfo.publisher + ', ' + publishedYear[1] + ' - ' + book.volumeInfo.pageCount + ' páginas</span></p>'
                        +'<div class="links">'
                            +'<a href="'+ book.volumeInfo.infoLink +'" class="link-book-info">Ver info completa en Google</a>'
                            +'<a href="'+ book.volumeInfo.previewLink +'" class="link-book-info">Ver preview en Google</a>'
                        +'</div>'
                        +'<p class="descripcion">' + book.volumeInfo.description  +'</p>'
                        +'<a class="añadirLibro waves-effect waves-light btn" onclick="addLibroToLibreria('+book.id+')" class="waves-effect waves-light btn">Añadir</a>'
                    +'</div>'
                    +'</li>');


                if(book.volumeInfo.averageRating)   // En caso de que tenga puntuacion se la mostramos
                    $('#books-list ul.collapsible li:last-child').find('.descripcion').append(calcularCodigoCreacionValoracion(book.volumeInfo.averageRating));
                else            // En caso negativo, enseñamos un mensaje diciendolo
                    $('#books-list ul.collapsible li:last-child').find('.publicacion').append('<p>No hay valoración</p>');
            });
        }
    });
}


// Calculamos el codigo de la valoracion de dicho libro
function calcularCodigoCreacionValoracion(rate)
{
    var sValoracion = '<span class="valoracion">';
    var numEstrellasLLenas = Math.floor(rate);                              // Estrellas llenas
    var numEstrellasVacias = Math.floor(5-rate);                            // Estrellas vacias
    var numEstrellasMedias = 5 - numEstrellasLLenas - numEstrellasVacias;   // Estrellas medio llenas

    for(var i=0; i<numEstrellasLLenas; i++) {   sValoracion += '<span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>';}
    for(var i=0; i<numEstrellasMedias; i++) {   sValoracion += '<span class="half-star"><i class="fa fa-star-half-o" aria-hidden="true"></i></span>';}
    for(var i=0; i<numEstrellasVacias; i++) {   sValoracion += '<span class="no-star"><i class="fa fa-star-o" aria-hidden="true"></i></span>';}
    sValoracion += '</span>';
    insertarCodigoValoracion(sValoracion);
}


// Insertamos el codigo de valoracion dentro de la estructura del libro
function insertarCodigoValoracion(codigo)
{
    $('#books-list ul.collapsible li:last-child').find('.publicacion').after(codigo);
}


