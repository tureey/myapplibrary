$(document).ready(function(){
    var $navMain = $('#main-nav-container');
    $('#trigger-mobile-menu').on('click', function(){
        if($navMain.is(':visible'))
            $navMain.slideUp('fast');
        else
            $navMain.slideDown('300');
    });
});