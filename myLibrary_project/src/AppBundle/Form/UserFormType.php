<?php

namespace AppBundle\Form;


use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            array(
                'label' => 'Nombre',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Obligatorio')),
                )
            )
        );

        $builder->add(
            'lastname',
            TextType::class,
            array(
                'label' => 'Apellido',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Obligatorio')),
                )
            )
        );

        $builder->add(
            'username',
            TextType::class,
            array(
                'label' => 'Apodo de usuario',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Obligatorio')),
                )
            )
        );


        $builder->add(
            'email',
            TextType::class,
            array(
                'label' => 'Correo electronico',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Obligatorio')),
                )
            )
        );

        $builder->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Contraseña'),
                'second_options' => array('label' => 'Repetir contraseña'),
                'required' => true,
            )
        );

    }

    public function getName()
    {
        return 'user';
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle/Entity/Users',
            'csrf_protection'	 => true,
            'csrf_field_name'	 => '_token',
        ));
    }
}