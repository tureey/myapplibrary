<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class SocialFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            array(
                'label' => 'Nombre',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Obligatorio')),
                )
            )
        );

        $builder->add(
            'url',
            TextType::class,
            array(
                'label' => '/url',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Obligatorio')),
                )
            )
        );

        $builder->add(
            'icon',
            TextType::class,
            array(
                'label' => 'Icono',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Obligatorio')),
                )
            )
        );

    }

    public function getName()
    {
        return 'social';
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle/Entity/Social',
            'csrf_protection'	 => true,
            'csrf_field_name'	 => '_token',
        ));
    }
}