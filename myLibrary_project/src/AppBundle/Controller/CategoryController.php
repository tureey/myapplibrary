<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManager;
use AppBundle\Controller\BaseController;


class CategoryController extends BaseController
{
    /**
     * Usado para renderizar de manera dinámica el menu para móvil
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderTopNavAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categoriesDynamic = $em->getRepository('AppBundle:Category')
                                ->findBy(array('isActive'=>true), array('position' => 'ASC'));

        $socialsDynamic = $em->getRepository('AppBundle:Social')
                                ->findBy(array('isActive'=>true), array('position' => 'ASC'));

        $this->addData('socials', $socialsDynamic);
        $this->addData('categories', $categoriesDynamic);
        return $this->render('AppBundle:menu:nav-top.html.twig', $this->getData());
    }

    /**
     * Usado para renderizar de manera dinámica el menu para dispositivos grandes
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderMainNavAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categoriesDynamic = $em->getRepository('AppBundle:Category')
            ->findBy(array('isActive'=>true), array('position' => 'ASC'));
        $socialsDynamic = $em->getRepository('AppBundle:Social')
            ->findBy(array('isActive'=>true), array('position' => 'ASC'));

        $this->addData('socials', $socialsDynamic);
        $this->addData('categories', $categoriesDynamic);
        return $this->render('AppBundle:menu:nav-main.html.twig', $this->getData());
    }
}