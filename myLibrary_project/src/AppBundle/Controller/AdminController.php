<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Admin;
use AppBundle\Entity\Users;
use AppBundle\Entity\Category;
use AppBundle\Entity\Social;
use AppBundle\Form\CategoryFormType;
use AppBundle\Form\SocialFormType;
use AppBundle\Form\UserFormType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;


class AdminController extends BaseController
{
    /**
     *  @Route("/admin", name="admin_index")
     */
    public function adminAction()
    {
        $this->addData('sectionTitle', 'Indice del dashboard');
        return $this->render('AppBundle:admin:index.html.twig', $this->getData());
    }



    /**
     *  @Route("/admin/create/user", name="admin_create_user")
     */
    public function crearUsuario(Request $request)
    {
        $user   = new Users();
        $em     = $this->getDoctrine()->getManager();

        $em->persist($user);

        $form   = $this->createForm(UserFormType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('admin_create_user');
        }

        $this->addData('formUser', $form->createView());
        $this->addData('sectionTitle', 'Creación de usuario');

        return $this->render('AppBundle:admin:user/createUser.html.twig', $this->getData());
    }


    /**
     *  @Route("/admin/list/user", name="admin_list_user")
     */
    public function listarUsuario()
    {
        $em     = $this->getDoctrine()->getManager();
        $usersRepo  = $em->getRepository('AppBundle:Users')->findAll();

        $this->addData('sectionTitle', 'Lista de usuarios');
        $this->addData('allUsers', $usersRepo);
        return $this->render('AppBundle:admin:user/listUser.html.twig', $this->getData());
    }

    /**
     * @Route ("/admin/edit/user/{id}", name="admin_edit_user")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editUser(Request $request, $id)
    {


        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:Users')->find($id);

        if(!$user instanceof Users){
            $message = "ERROR incorrect user";
            print_r($message);
            die();
        }
        $form = $this->createForm(UserFormType::class, $user)->remove("plainPassword");
        // Realizamos el hadle de los datos del formulario
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('admin_list_user');

        }

        $this->addData('edit', true);
        $this->addData('formUser', $form->createView());
        $this->addData('sectionTitle', 'Lista de usuarios');
        return $this->render('AppBundle:admin:user/createUser.html.twig', $this->getData());
    }

    /**
     * @Route("/admin/delete/user", name="admin_delete_user")
     */
    public function deleteUsuario(Request $request)
    {
        $id = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:Users')->find($id);

        if (empty($user)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $em->remove($user);
        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }



    /**
     *  @Route("/admin/create/admin", name="admin_create_admin")
     */
    public function crearAdmin(Request $request)
    {
        $admin   = new Admin();
        $em     = $this->getDoctrine()->getManager();

        $em->persist($admin);

        $form   = $this->createForm(UserFormType::class, $admin);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $password = $this->get('security.password_encoder')->encodePassword($admin, $admin->getPlainPassword());
            $admin->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('admin_create_admin');
        }

        $this->addData('formAdmin', $form->createView());
        $this->addData('sectionTitle', 'Creación de administrador');

        return $this->render('AppBundle:admin:admin/createAdmin.html.twig', $this->getData());
    }


    /**
     *  @Route("/admin/list/admin", name="admin_list_admin")
     */
    public function listarAdmin()
    {
        $em     = $this->getDoctrine()->getManager();
        $adminsRepo  = $em->getRepository('AppBundle:Admin')->findAll();

        $this->addData('sectionTitle', 'Lista de administradores');
        $this->addData('allAdmins', $adminsRepo);
        return $this->render('AppBundle:admin:admin/listAdmin.html.twig', $this->getData());
    }

    /**
     * @Route("/admin/delete/admin", name="admin_delete_admin")
     */
    public function deleteAdmin(Request $request)
    {
        $id = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $admin = $em->getRepository('AppBundle:Admin')->find($id);

        if (empty($admin)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $em->remove($admin);
        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }



    /**
     *  @Route("/admin/list/book", name="admin_list_book")
     */
    public function listarBook()
    {
        $em     = $this->getDoctrine()->getManager();
        $booksRepo  = $em->getRepository('AppBundle:Book')->findAll();

        $this->addData('sectionTitle', 'Lista de Libros');
        $this->addData('allBooks', $booksRepo);
        return $this->render('AppBundle:admin:book/listBook.html.twig', $this->getData());
    }

    /**
     * @Route("/admin/delete/book", name="admin_delete_book")
     */
    public function deleteBook(Request $request)
    {
        $id = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('AppBundle:Book')->find($id);

        if (empty($book)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $em->remove($book);
        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }



    /**
     *  @Route("/admin/create/category", name="admin_create_category")
     */
    public function crearCategory(Request $request)
    {
        $category   = new Category();

        $em     = $this->getDoctrine()->getManager();

        $em->persist($category);

        $form   = $this->createForm(CategoryFormType::class, $category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('admin_create_category');
        }

        $this->addData('formCategory', $form->createView());
        $this->addData('sectionTitle', 'Creación de categoria');

        return $this->render('AppBundle:admin:category/createCategory.html.twig', $this->getData());
    }


    /**
     *  @Route("/admin/list/category", name="admin_list_category")
     */
    public function listarCategory()
    {
        $em     = $this->getDoctrine()->getManager();
        $categoryRepo  = $em->getRepository('AppBundle:Category')->findAll();

        $this->addData('sectionTitle', 'Lista de categorias');
        $this->addData('allCategories', $categoryRepo);
        return $this->render('AppBundle:admin:category/listCategory.html.twig', $this->getData());
    }

    /**
     * @Route("/admin/delete/category", name="admin_delete_category")
     */
    public function deleteCategory(Request $request)
    {
        $id = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->find($id);

        if (empty($category)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $em->remove($category);
        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }



    /**
     *  @Route("/admin/create/social", name="admin_create_social")
     */
    public function crearSocial(Request $request)
    {
        $social   = new Social();

        $em     = $this->getDoctrine()->getManager();

        $em->persist($social);

        $form   = $this->createForm(SocialFormType::class, $social);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('admin_create_social');
        }

        $this->addData('formSocial', $form->createView());
        $this->addData('sectionTitle', 'Creación de red social');

        return $this->render('AppBundle:admin:social/createSocial.html.twig', $this->getData());
    }


    /**
     *  @Route("/admin/list/social", name="admin_list_social")
     */
    public function listarSocial()
    {
        $em     = $this->getDoctrine()->getManager();
        $socialsRepo  = $em->getRepository('AppBundle:Social')->findAll();

        $this->addData('sectionTitle', 'Lista de redes sociales');
        $this->addData('allSocials', $socialsRepo);
        return $this->render('AppBundle:admin:social/listSocial.html.twig', $this->getData());
    }

    /**
     * @Route("/admin/delete/social", name="admin_delete_social")
     */
    public function deleteSocial(Request $request)
    {
        $id = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $social = $em->getRepository('AppBundle:Social')->find($id);

        if (empty($social)) {
            $this->sendResponseStatus('ERROR');

            return new JSONResponse($this->getData());
        }

        $em->remove($social);
        $em->flush();

        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

    /**
     *  @Route("/admin/order/social", name="admin_order_social")
     */
    public function showSocialOrder()
    {
        $em     = $this->getDoctrine()->getManager();
        $socialsRepo  = $em->getRepository('AppBundle:Social')->findBy(array("position" => "ASC"));

        $this->addData('sectionTitle', 'Ordenar las redes sociales');
        $this->addData('allSocials', $socialsRepo);
        return $this->render('AppBundle:admin:social/editSocialOrder.html.twig', $this->getData());
    }

    /**
     * @Route("/admin/update/order/social", name="admin_update_social_position")
     */
    public function orderSocial(Request $request)
    {
        $em             = $this->getDoctrine()->getManager();
        $socialSorted   = $request->request->get('socialSorted');
        $numElementos   = count($socialSorted);


        if (empty($socialSorted))
        {
            $this->sendResponseStatus('ERROR');
            return new JSONResponse($this->getData());
        }


        for($i=0; $i<$numElementos; $i++)
        {
            $social = $em->getRepository('AppBundle:Social')->findById($socialSorted[$i]);
            $social[0]->setPosition($i+1); // Social[0] porque nos devuelve un arrayCollection
            $em->persist($social[0]);
        }


        $em->flush();
        $this->sendResponseStatus('OK');

        // Generamos los datos para la respuesta ajax
        return new JSONResponse($this->getData());
    }

}