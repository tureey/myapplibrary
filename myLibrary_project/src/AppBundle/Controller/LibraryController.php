<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



class LibraryController extends BaseController
{
    private $APIkey = "&key=" . "AIzaSyAN638WbYe1vOfGya989p7ZbfXnPzBLfkg";

    /**
     * @Route ("/libreria", name="libreria")
     */
    public function renderLibreria(){

        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $em = $this->getDoctrine()->getManager();

            $user = $this->get('security.token_storage')->getToken()->getUser();

            $this->addData('user', $user);
            return $this->render('AppBundle:libreria:libreria.html.twig', $this->getData());
        }

        return $this->redirect("login");
    }

    /**
     * Adds to the library the requested book
     * @Route ("/libreria/addBook/{id_google}", name="addBook")
     */
    public function addBook($id_google)
    {
        $securityContext = $this->container->get('security.authorization_checker');

        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $em   = $this->getDoctrine()->getManager();
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $respuestaApi   = $this->getBookFromAPI($id_google); // Cogemos toda la información del libro en cuestión

            // Para ahorrarnos código y usar este subarray de manera practica
            // Comprobamos si tenemos el libro en la base de datos para ahorrarnos una consulta a la API
            $vInfo      = $respuestaApi['volumeInfo'];
            $bookExist  = $em->getRepository('AppBundle:Book')->findBy(array('id_google'=>$id_google));

            if($bookExist) // Si existe el libro, miramos si este usuario ya lo tiene o no.
            {
                $librosUsuario  = $user->getBooks();
                $tieneLibro     = false; // Variable para saber si tiene el libro o no

                foreach($librosUsuario as $libro){
                    if($libro->getIdGoogle() == $id_google) { // Si el usuario lo tiene sacamos un mensaje de error
                        $tieneLibro = true;
                    }
                }

                if($tieneLibro === true) { // SI que tiene el libro. Sacamos mensaje de error
                    $this->sendResponseStatus('REPEATED');
                }else {
                    $user->addBook($bookExist[0]);
                    $em->persist($user);
                    $em->flush();

                    $this->sendResponseStatus('OK');
                }


            }

            else // Si el libro no consta en la aplicación hacemos peticion a la API
            {
                $book = new Book();

                $book->setTitle($vInfo['title']);
                $book->setAuthor($vInfo['authors'][0]);
                $book->setLanguage($vInfo['language']);
                $book->setGenre($vInfo['categories'][0]);

                if(array_key_exists('retailPrice', $respuestaApi['saleInfo'])) {
                    if (array_key_exists('amount', $respuestaApi['saleInfo']['retailPrice'])) {
                        $book->setPrice($respuestaApi['saleInfo']['retailPrice']['amount']);
                    }
                }
                $book->setPages($vInfo['printedPageCount']);
                $book->setSynopsis($vInfo['description']);

                if(array_key_exists('averageRating', $vInfo)){
                    $book->setRate($vInfo['averageRating']);
                }
                $book->setIdGoogle($id_google);


                $em->persist($book);    // Guardamos el libro en la bbdd
                $user->addBook($book);  // Añadimos el libro al usuario
                $em->persist($user);    // Actualizamos el usuario
                $em->flush();

                $this->sendResponseStatus('OK');
            }

            // Generamos los datos para la respuesta ajax
            return new JSONResponse($this->getData());
        }

        return $this->redirect("login");
    }

    /**
     * Returns all the user books
     * @Route ("/libreria/getUserBooks", name="get_userBooks")
     */
    function getBooksLibrary(){
        $securityContext = $this->container->get('security.authorization_checker');

        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $em   = $this->getDoctrine()->getManager();
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $userBooks = $user->getBooks();

            $this->addData('userBooks', $userBooks);
            json_encode($userBooks);

            // Generamos los datos para la respuesta ajax
            return new JSONResponse($this->getData());
        }

        return $this->redirect("login");
    }

    // Coge la información de un libro gracias a su id
    public function getBookFromAPI($id){
        // Instancia del BuzzBundle para peticiones HTTP externas
        $buzz = $this->container->get('buzz');

        // Resultado de la peticion HTTP 'GET'
        $responseAPI = $buzz->get('https://www.googleapis.com/books/v1/volumes/'.$id);
        $jsonLibro = $responseAPI->getContent();

        return $this->transformarStringToArray($jsonLibro);
    }


    // Se usa para hacer que el String devuelto por la API de Google sea un array
    public function transformarStringToArray($string){
        $stringDecoded = json_decode($string);
        $array = json_decode(json_encode($stringDecoded), true);

        return $array;

    }
}

