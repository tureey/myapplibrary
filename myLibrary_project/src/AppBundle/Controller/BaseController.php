<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/*
 * Controllador base de nuestra aplicacion
 */

class BaseController extends Controller{
    /**
     * @var $data
     */
    protected $data;

    /**
     * Enviar estado para el handleReponse de Javascript
     */
    protected function sendResponseStatus($status)
    {
        $all_status = array('OK', 'ERROR', 'WARNING', ' NO_AUTHORIZED', 'NOT_FOUND', 'REPEATED');

        if (!in_array($status, $all_status)) {
            $status = 'ERROR';
        }

        $this->addData('type', $status);
    }

    /**
     *  Añade datos que se enviarán a la vista
     * @param string $key
     * @param $value
     */
    protected function addData($key, $value)
    {
        $this->data[$key] = $value;
    }

    /**
     * Devuelve los datos que se enviarán a la vista.Funcion getData
     *
     * @return array
     */
    protected function getData ()
    {
        return $this->data;
    }
}