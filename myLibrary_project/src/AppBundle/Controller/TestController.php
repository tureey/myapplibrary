<?php


namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\CategoryFormType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;


class TestController extends BaseController
{
    /**
     * @Route ("/create/category", name="create_category")
     */
    public function createCategory(Request $request){

        $category   = new Category();

        $em     = $this->getDoctrine()->getManager();

        $em->persist($category);

        $form   = $this->createForm(CategoryFormType::class, $category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('create_category');
        }

        $this->addData('formCategory', $form->createView());
        return $this->render('AppBundle:test:createCategory.html.twig', $this->getData());
    }
}