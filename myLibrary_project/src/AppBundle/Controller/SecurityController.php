<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Admin;
use AppBundle\Entity\Users;
use AppBundle\Form\AdminFormType;
use AppBundle\Form\UserFormType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;


class SecurityController extends BaseController
{

    /**
     * @Route("/signup", name="signup")
     * @param Request $request
     * @return response
     */
    public function guardarUser(Request $request)
    {
        $securityContext = $this->container->get('security.authorization_checker');

        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $em = $this->getDoctrine()->getManager();

            $user = $this->get('security.token_storage')->getToken()->getUser();

            $this->addData('user', $user);
            return $this->redirect("perfil");
        }

        else
        {
            $user   = new Users();

            $em     = $this->getDoctrine()->getManager();

            $em->persist($user);

            $form   = $this->createForm(UserFormType::class, $user);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
                $em = $this->getDoctrine()->getManager();
                $em->flush();

                return $this->redirectToRoute('signup');
            }

            $this->addData('formUser', $form->createView());
            return $this->render('AppBundle:signup:signup.html.twig', $this->getData());
        }
    }

    /**
     * @Route("/login", name="login_user")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showLogin(Request $request)
    {
        $securityContext = $this->container->get('security.authorization_checker');

        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $em = $this->getDoctrine()->getManager();

            $user = $this->get('security.token_storage')->getToken()->getUser();

            $this->addData('user', $user);
            return $this->redirect("perfil");
        }

        else
        {
            $authenticationUtils = $this->get('security.authentication_utils');

            // get the login error if there is one
            $error = $authenticationUtils->getLastAuthenticationError();

            // last username entered by the user
            $lastUsername = $authenticationUtils->getLastUsername();

            $this->addData('last_username', $lastUsername);
            $this->addData('error', $error);
            return $this->render('AppBundle:login:login.html.twig', $this->getData());
        }
    }

    /**
     * @Route("/create/admin", name="create_admin")
     * @param Request $request
     * @return response
     */
   /* public function guardarAdmin(Request $request)
    {
        $admin   = new Admin();

        $em     = $this->getDoctrine()->getManager();

        $em->persist($admin);

        $form   = $this->createForm(UserFormType::class, $admin);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $password = $this->get('security.password_encoder')->encodePassword($admin, $admin->getPlainPassword());
            $admin->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('signup');
        }

        $this->addData('formUser', $form->createView());
        return $this->render('AppBundle:signup:signup.html.twig', $this->getData());
    }*/
}