# My appLibrary #
## Descripción ##
**Versión 1.0:** Aplicación web para "digitalizar" tu biblioteca, introduce todos tus libros de manera fácil y práctica para recibir recomendaciones y estadísticas del contenido comprado (idioma, género más leídos, etc..).

**Versión 2.0:** Añadido el valor correspondiente a la aplicación para poder seleccionar que libro queremos incorporar al mercado para venderlo como libro de segunda mano y así realizar una aplicación de venta de segunda mano únicamente de libros.

## Recursos ##
**Información de los libros:** Usando Google API Books [Pincha aquí](https://developers.google.com/books/docs/v1/getting_started)

## Tecnologías ##
**sass:** Preprocesador CSS [Pincha aquí](http://sass-lang.com/)